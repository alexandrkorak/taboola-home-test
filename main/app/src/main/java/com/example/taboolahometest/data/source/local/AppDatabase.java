package com.example.taboolahometest.data.source.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {FeedLocal.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract FeedDao feedDao();
}
