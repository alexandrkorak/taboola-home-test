package com.example.taboolahometest.data.source.local;

import androidx.annotation.NonNull;

import com.example.taboolahometest.data.source.Feed;

import java.util.LinkedList;
import java.util.List;

public class FeedLocalMapper {
    @NonNull
    public Feed fromLocal(@NonNull final FeedLocal feedLocal) {
        return new Feed(
                feedLocal.getThumbnail(),
                feedLocal.getName(),
                feedLocal.getDescription()
        );
    }

    @NonNull
    public FeedLocal toLocal(@NonNull final Feed feed) {
        final FeedLocal feedLocal = new FeedLocal();
        feedLocal.setThumbnail(feed.getThumbnail());
        feedLocal.setName(feed.getName());
        feedLocal.setDescription(feed.getDescription());
        return feedLocal;
    }

    @NonNull
    public List<Feed> fromLocals(@NonNull final List<FeedLocal> feedLocals) {
        final List<Feed> feeds = new LinkedList<>();
        for (final FeedLocal feedLocal : feedLocals) {
            feeds.add(fromLocal(feedLocal));
        }
        return feeds;
    }

    @NonNull
    public List<FeedLocal> toLocals(@NonNull final List<Feed> feeds) {
        final List<FeedLocal> feedLocals = new LinkedList<>();
        for (final Feed feed : feeds) {
            feedLocals.add(toLocal(feed));
        }
        return feedLocals;
    }
}
