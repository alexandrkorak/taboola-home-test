package com.example.taboolahometest.ui.feed;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taboolahometest.data.source.Feed;
import com.example.taboolahometest.databinding.ItemFeedBinding;
import com.example.taboolahometest.databinding.ItemTaboolaBinding;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.FeedsViewHolder> {
    public static final int FEED_ITEM = 0;
    public static final int ADS_ITEM = 1;
    private static final String TYPE_IS_NOT_SUPPORTED_ERROR = "Type is not supported by feeds adapter";
    private final List<FeedsItem> items;

    public FeedsAdapter(@NonNull final List<FeedsItem> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public FeedsViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == FEED_ITEM) {
            final ItemFeedBinding binding = ItemFeedBinding.inflate(layoutInflater, parent, false);
            return new FeedViewHolder(binding);
        } else if (viewType == ADS_ITEM) {
            final ItemTaboolaBinding binding = ItemTaboolaBinding.inflate(layoutInflater, parent, false);
            return new TaboolaViewHolder(binding);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final FeedsViewHolder holder, final int position) {
        final int viewType = getItemViewType(position);
        if (viewType == FEED_ITEM) {
            final FeedItem feedItem = (FeedItem) items.get(position);
            ((FeedViewHolder) holder).bind(feedItem.getFeed());
        } else if (viewType == ADS_ITEM) {
            final TaboolaItem taboolaItem = (TaboolaItem) items.get(position);
            ((TaboolaViewHolder) holder).bind(taboolaItem);
        } else {
            throw new IllegalArgumentException(TYPE_IS_NOT_SUPPORTED_ERROR);
        }
    }

    @Override
    public int getItemViewType(int position) {
        final FeedsItem item = items.get(position);
        if (item instanceof FeedItem) {
            return FEED_ITEM;
        } else if (item instanceof TaboolaItem) {
            return ADS_ITEM;
        } else {
            throw new IllegalArgumentException(TYPE_IS_NOT_SUPPORTED_ERROR);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class FeedsViewHolder extends RecyclerView.ViewHolder {
        FeedsViewHolder(@NonNull final View view) {
            super(view);
        }
    }

    static class FeedViewHolder extends FeedsViewHolder {
        private final ItemFeedBinding binding;

        public FeedViewHolder(@NonNull final ItemFeedBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(@NonNull final Feed feed) {
            binding.name.setText(feed.getName());
            binding.description.setText(feed.getDescription());
            Picasso.get().load(feed.getThumbnail()).into(binding.thumbnail);
        }
    }

    static class TaboolaViewHolder extends FeedsViewHolder {
        private final ItemTaboolaBinding binding;

        public TaboolaViewHolder(@NonNull final ItemTaboolaBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(@NonNull final TaboolaItem taboolaItem) {
            binding.taboolaView.setPublisher(taboolaItem.getPublisherId())
                    .setMode(taboolaItem.getModeId())
                    .setPlacement(taboolaItem.getPlacementId())
                    .setPageType(taboolaItem.getType())
                    .setTargetType(taboolaItem.getTarget())
            .setPageUrl("google.com");

            binding.taboolaView.fetchContent();
        }
    }
}
