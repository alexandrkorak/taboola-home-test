package com.example.taboolahometest.ui.feed;

import androidx.annotation.NonNull;

import java.util.Objects;

public class TaboolaItem implements FeedsItem {
    @NonNull
    private final String publisherId;
    @NonNull
    private final String modeId;
    @NonNull
    private final String placementId;
    @NonNull
    private final String type;
    @NonNull
    private final String target;
    @NonNull
    private final String pageUrl;

    public TaboolaItem(
            @NonNull final String publisherId, @NonNull final String modeId,
            @NonNull final String placementId, @NonNull final String type,
            @NonNull final String target, @NonNull final String pageUrl
    ) {
        this.publisherId = publisherId;
        this.modeId = modeId;
        this.placementId = placementId;
        this.type = type;
        this.target = target;
        this.pageUrl = pageUrl;
    }

    @NonNull
    public String getPublisherId() {
        return publisherId;
    }

    @NonNull
    public String getModeId() {
        return modeId;
    }

    @NonNull
    public String getPlacementId() {
        return placementId;
    }

    @NonNull
    public String getType() {
        return type;
    }

    @NonNull
    public String getTarget() {
        return target;
    }

    @NonNull
    public String getPageUrl() {
        return pageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaboolaItem that = (TaboolaItem) o;
        return publisherId.equals(that.publisherId) && modeId.equals(that.modeId)
                && placementId.equals(that.placementId) && type.equals(that.type)
                && target.equals(that.target) && pageUrl.equals(that.pageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(publisherId, modeId, placementId, type, target, pageUrl);
    }
}
