package com.example.taboolahometest.data.source.remote;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FeedRemote {

    @JsonProperty("thumbnail")
    private String thumbnail;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(final String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return
                "FeedRemoteItem{" +
                        "thumbnail = '" + thumbnail + '\'' +
                        ",name = '" + name + '\'' +
                        ",description = '" + description + '\'' +
                        "}";
    }
}