package com.example.taboolahometest.ui;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.taboolahometest.data.source.Feed;
import com.example.taboolahometest.data.source.FeedRepository;
import com.example.taboolahometest.ui.feed.FeedsItem;
import com.example.taboolahometest.ui.feed.decorator.TaboolaFeedsDecorator;

import java.util.List;
import java.util.concurrent.ExecutorService;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class MainViewModel extends ViewModel {

    @NonNull
    private final ExecutorService executorService;
    @NonNull
    private final FeedRepository feedRepository;
    @NonNull
    private final TaboolaFeedsDecorator taboolaFeedsDecorator;

    @NonNull
    private final MutableLiveData<MainViewState> viewState = new MutableLiveData<>();

    @Inject
    MainViewModel(
            @NonNull final ExecutorService executorService,
            @NonNull final FeedRepository feedRepository,
            @NonNull final TaboolaFeedsDecorator taboolaFeedsDecorator
    ) {
        this.executorService = executorService;
        this.feedRepository = feedRepository;
        this.taboolaFeedsDecorator = taboolaFeedsDecorator;
        loadItems();
    }

    private void loadItems() {
        viewState.setValue(new Loading());
        executorService.execute(() -> {
            try {
                final List<Feed> feeds = feedRepository.getFeeds();
                final List<FeedsItem> feedsItems = taboolaFeedsDecorator.decorateWithTaboola(feeds);
                viewState.postValue(new Success(feedsItems));
            } catch (final Exception e) {
                e.printStackTrace();
                viewState.postValue(new Error(e));
            }
        });
    }

    @NonNull
    public LiveData<MainViewState> getViewState() {
        return viewState;
    }

    @Override
    protected void onCleared() {
        executorService.shutdownNow();
    }

    public void retry() {
        loadItems();
    }
}
