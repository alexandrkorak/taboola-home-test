package com.example.taboolahometest.data.source.remote;

import androidx.annotation.NonNull;

import com.example.taboolahometest.data.source.Feed;

import java.util.LinkedList;
import java.util.List;

public class FeedRemoteMapper {
    @NonNull
    public Feed fromRemote(@NonNull final FeedRemote feedRemote) {
        return new Feed(
                feedRemote.getThumbnail(),
                feedRemote.getName(),
                feedRemote.getDescription()
        );
    }

    @NonNull
    public List<Feed> fromRemotes(@NonNull final List<FeedRemote> feedRemotes) {
        final List<Feed> feeds = new LinkedList<>();
        for (final FeedRemote feedRemote : feedRemotes) {
            feeds.add(fromRemote(feedRemote));
        }
        return feeds;
    }
}
