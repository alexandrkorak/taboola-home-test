package com.example.taboolahometest.data.source.local;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class FeedLocal {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "thumbnail")
    public String thumbnail;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "description")
    public String description;

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(final String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return
                "FeedLocalItem{" +
                        "thumbnail = '" + thumbnail + '\'' +
                        ",name = '" + name + '\'' +
                        ",description = '" + description + '\'' +
                        "}";
    }
}
