package com.example.taboolahometest.data.source;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.util.List;

public class FeedRepository implements FeedDataSource {
    @NonNull
    private final FeedMutableDataSource localDataSource;
    @NonNull
    private final FeedDataSource remoteDataSource;

    public FeedRepository(
            @NonNull final FeedMutableDataSource localDataSource,
            @NonNull final FeedDataSource remoteDataSource
    ) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    @NonNull
    @Override
    public List<Feed> getFeeds() throws IOException {
        final List<Feed> localFeeds = localDataSource.getFeeds();
        if (!localFeeds.isEmpty()) {
            return localFeeds;
        }

        final List<Feed> remoteFeeds = remoteDataSource.getFeeds();
        localDataSource.setFeeds(remoteFeeds);
        return remoteFeeds;
    }
}
