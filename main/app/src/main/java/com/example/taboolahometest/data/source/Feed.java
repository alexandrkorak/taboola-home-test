package com.example.taboolahometest.data.source;

public class Feed {

    private String thumbnail;
    private String name;
    private String description;

    public Feed(final String thumbnail, final String name, final String description) {
        this.thumbnail = thumbnail;
        this.name = name;
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(final String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return
                "FeedItem{" +
                        "thumbnail = '" + thumbnail + '\'' +
                        ",name = '" + name + '\'' +
                        ",description = '" + description + '\'' +
                        "}";
    }
}