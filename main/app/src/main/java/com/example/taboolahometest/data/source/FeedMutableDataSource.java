package com.example.taboolahometest.data.source;

import androidx.annotation.NonNull;

import java.util.List;

public interface FeedMutableDataSource extends FeedDataSource {
    void setFeeds(@NonNull final List<Feed> feeds);
}
