package com.example.taboolahometest.ui.feed.decorator;

import androidx.annotation.NonNull;

import com.example.taboolahometest.ui.feed.TaboolaItem;

public interface TaboolaItemProvider {
    int getFirstItemPosition();

    @NonNull
    TaboolaItem getFirstItem();

    int getLastItemPosition();

    @NonNull
    TaboolaItem getLastItem();
}
