package com.example.taboolahometest.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.taboolahometest.R;
import com.example.taboolahometest.databinding.ActivityMainBinding;
import com.example.taboolahometest.ui.feed.FeedsAdapter;
import com.example.taboolahometest.ui.feed.FeedsItem;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    private MainViewModel mainViewModel;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setUpList();

        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        mainViewModel.getViewState().observe(this, this::handleViewState);
    }

    private void setUpList() {
        binding.list.setLayoutManager(new LinearLayoutManager(this));
        binding.list.setHasFixedSize(true);
        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                this, LinearLayout.VERTICAL);
        binding.list.addItemDecoration(dividerItemDecoration);
        binding.list.setNestedScrollingEnabled(true);
    }

    private void handleLoading() {
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    private void handleError(final Exception exception) {
        binding.progressBar.setVisibility(View.GONE);
        final Snackbar snackbar = Snackbar.make(
                binding.getRoot(), R.string.something_went_wrong, BaseTransientBottomBar.LENGTH_INDEFINITE)
                .setAction(R.string.retry, view -> retry());
        snackbar.show();
        Log.e(TAG, "Error loading feeds", exception);
    }

    private void retry() {
        mainViewModel.retry();
    }

    private void handleSuccess(final List<FeedsItem> items) {
        binding.list.setAdapter(new FeedsAdapter(items));
        binding.list.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
    }

    private void handleViewState(final MainViewState viewState) {
        if (viewState instanceof Loading) {
            handleLoading();
        } else if (viewState instanceof Error) {
            handleError(((Error) viewState).getException());
        } else if (viewState instanceof Success) {
            handleSuccess(((Success) viewState).getFeedItems());
        } else {
            throw new IllegalStateException();
        }
    }
}