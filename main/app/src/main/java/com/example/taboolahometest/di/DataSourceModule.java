package com.example.taboolahometest.di;

import static com.example.taboolahometest.data.source.remote.FeedApi.HOST_API;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import com.example.taboolahometest.data.source.FeedDataSource;
import com.example.taboolahometest.data.source.FeedMutableDataSource;
import com.example.taboolahometest.data.source.FeedRepository;
import com.example.taboolahometest.data.source.local.AppDatabase;
import com.example.taboolahometest.data.source.local.FeedDao;
import com.example.taboolahometest.data.source.local.FeedLocalDataSource;
import com.example.taboolahometest.data.source.local.FeedLocalMapper;
import com.example.taboolahometest.data.source.remote.FeedApi;
import com.example.taboolahometest.data.source.remote.FeedRemoteDataSource;
import com.example.taboolahometest.data.source.remote.FeedRemoteMapper;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Module
@InstallIn(SingletonComponent.class)
public interface DataSourceModule {

    @NonNull
    @Provides
    static String provideHostApi() {
        return HOST_API;
    }

    @NonNull
    @Provides
    static Converter.Factory provideRetrofitConverterFactory() {
        return JacksonConverterFactory.create();
    }

    @NonNull
    @Provides
    static Retrofit provideRetrofit(@NonNull final String hostApi, @NonNull final Converter.Factory converterFactory) {
        return new Retrofit.Builder()
                .baseUrl(hostApi)
                .addConverterFactory(converterFactory)
                .build();
    }

    @NonNull
    @Provides
    static FeedApi provideFeedApi(@NonNull final Retrofit retrofit) {
        return retrofit.create(FeedApi.class);
    }

    @NonNull
    @Provides
    static FeedRemoteMapper provideFeedRemoteMapper() {
        return new FeedRemoteMapper();
    }

    @NonNull
    @Provides
    static FeedDataSource provideFeedRemoteDataSource(
            @NonNull final FeedApi feedApi, @NonNull final FeedRemoteMapper feedRemoteMapper) {
        return new FeedRemoteDataSource(feedApi, feedRemoteMapper);
    }

    @NonNull
    @Provides
    static AppDatabase provideAppDatabase(@ApplicationContext final Context context) {
        return Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "database-name").build();
    }

    @NonNull
    @Provides
    static FeedDao provideFeedDao(@NonNull final AppDatabase appDatabase) {
        return appDatabase.feedDao();
    }

    @NonNull
    @Provides
    static FeedLocalMapper provideFeedLocalMapper() {
        return new FeedLocalMapper();
    }

    @NonNull
    @Provides
    static FeedMutableDataSource provideFeedLocalDataSource(
            @NonNull final FeedDao feedDao,
            @NonNull final FeedLocalMapper feedLocalMapper
    ) {
        return new FeedLocalDataSource(feedDao, feedLocalMapper);
    }

    @NonNull
    @Provides
    static FeedRepository provideFeedRepository(
            @NonNull final FeedMutableDataSource localDataSource,
            @NonNull final FeedDataSource remoteDataSource
    ) {
        return new FeedRepository(localDataSource, remoteDataSource);
    }
}
