package com.example.taboolahometest.di;

import androidx.annotation.NonNull;

import com.example.taboolahometest.ui.feed.decorator.DefaultTaboolaFeedsDecorator;
import com.example.taboolahometest.ui.feed.decorator.DefaultTaboolaItemProvider;
import com.example.taboolahometest.ui.feed.decorator.TaboolaFeedsDecorator;
import com.example.taboolahometest.ui.feed.decorator.TaboolaItemProvider;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ViewModelComponent;

@Module
@InstallIn(ViewModelComponent.class)
public interface AppModule {

    @NonNull
    @Provides
    static ExecutorService provideExecutorService() {
        return Executors.newSingleThreadExecutor();
    }

    @NonNull
    @Provides
    static TaboolaItemProvider provideTaboolaItemProvider() {
        return new DefaultTaboolaItemProvider();
    }

    @NonNull
    @Provides
    static TaboolaFeedsDecorator provideTaboolaFeedsDecorator(@NonNull final TaboolaItemProvider taboolaItemProvider) {
        return new DefaultTaboolaFeedsDecorator(taboolaItemProvider);
    }
}