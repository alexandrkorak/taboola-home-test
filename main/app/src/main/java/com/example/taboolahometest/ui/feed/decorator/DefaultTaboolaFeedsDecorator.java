package com.example.taboolahometest.ui.feed.decorator;

import androidx.annotation.NonNull;

import com.example.taboolahometest.data.source.Feed;
import com.example.taboolahometest.ui.feed.FeedItem;
import com.example.taboolahometest.ui.feed.FeedsItem;

import java.util.LinkedList;
import java.util.List;

public class DefaultTaboolaFeedsDecorator implements TaboolaFeedsDecorator {
    @NonNull
    private final TaboolaItemProvider taboolaItemProvider;

    public DefaultTaboolaFeedsDecorator(@NonNull TaboolaItemProvider taboolaItemProvider) {
        this.taboolaItemProvider = taboolaItemProvider;
    }

    @NonNull
    @Override
    public List<FeedsItem> decorateWithTaboola(@NonNull List<Feed> feeds) {
        final List<FeedsItem> items = new LinkedList<>();
        for (final Feed feed : feeds) {
            items.add(new FeedItem(feed));
        }
        final int firstItemPosition = Math.min(taboolaItemProvider.getFirstItemPosition() - 1, items.size());
        items.add(firstItemPosition, taboolaItemProvider.getFirstItem());

        final int secondItemPosition = Math.min(taboolaItemProvider.getLastItemPosition() - 1, items.size());
        items.add(secondItemPosition, taboolaItemProvider.getLastItem());
        return items;
    }
}
