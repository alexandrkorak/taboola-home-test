package com.example.taboolahometest.data.source.remote;

import androidx.annotation.NonNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface FeedApi {
    @NonNull
    String HOST_API = "https://s3-us-west-2.amazonaws.com/taboola-mobile-sdk/public/home_assignment/";

    @NonNull
    @GET("data.json")
    Call<List<FeedRemote>> loadFeeds();
}
