package com.example.taboolahometest.ui.feed.decorator;

import androidx.annotation.NonNull;

import com.example.taboolahometest.data.source.Feed;
import com.example.taboolahometest.ui.feed.FeedsItem;

import java.util.List;

public interface TaboolaFeedsDecorator {
    @NonNull
    List<FeedsItem> decorateWithTaboola(@NonNull final List<Feed> feeds);
}
