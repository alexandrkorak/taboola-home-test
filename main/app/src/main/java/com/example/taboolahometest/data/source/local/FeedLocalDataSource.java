package com.example.taboolahometest.data.source.local;

import androidx.annotation.NonNull;

import com.example.taboolahometest.data.source.Feed;
import com.example.taboolahometest.data.source.FeedMutableDataSource;

import java.util.ArrayList;
import java.util.List;

public class FeedLocalDataSource implements FeedMutableDataSource {
    private final List<Feed> feeds = new ArrayList<>();
    @NonNull
    private final FeedDao feedDao;
    @NonNull
    private final FeedLocalMapper feedLocalMapper;

    public FeedLocalDataSource(
            @NonNull final FeedDao feedDao,
            @NonNull final FeedLocalMapper feedLocalMapper
    ) {
        this.feedDao = feedDao;
        this.feedLocalMapper = feedLocalMapper;
    }

    @NonNull
    @Override
    public List<Feed> getFeeds() {
        if (!feeds.isEmpty()) return feeds;

        final List<FeedLocal> feedLocals = feedDao.getAll();
        feeds.addAll(feedLocalMapper.fromLocals(feedLocals));
        return feeds;
    }

    @Override
    public void setFeeds(@NonNull final List<Feed> feeds) {
        this.feeds.addAll(feeds);
        final List<FeedLocal> feedLocals = feedLocalMapper.toLocals(feeds);
        feedDao.insertAll(feedLocals.toArray(new FeedLocal[0]));
    }
}
