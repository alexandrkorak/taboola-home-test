package com.example.taboolahometest.ui.feed.decorator;

import androidx.annotation.NonNull;

import com.example.taboolahometest.ui.feed.TaboolaItem;

public class DefaultTaboolaItemProvider implements TaboolaItemProvider {
    @Override
    public int getFirstItemPosition() {
        return 3;
    }

    @NonNull
    @Override
    public TaboolaItem getFirstItem() {
        return createTaboolaItem("sdk-tester", "alternating-widget-without-video",
                "Below Article", "article", "mix", "google.com");
    }

    @Override
    public int getLastItemPosition() {
        return 10;
    }

    @NonNull
    @Override
    public TaboolaItem getLastItem() {
        return createTaboolaItem("sdk-tester", "thumbs-feed-01",
                "Feed without video", "article", "mix", "google.com");
    }

    private TaboolaItem createTaboolaItem(
            @NonNull final String publisherId, @NonNull final String modeId,
            @NonNull final String placementId, @NonNull final String type,
            @NonNull final String target, @NonNull final String pageUrl
    ) {
        return new TaboolaItem(publisherId, modeId, placementId, type, target, pageUrl);
    }
}
