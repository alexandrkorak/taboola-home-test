package com.example.taboolahometest.ui;

import androidx.annotation.NonNull;

import com.example.taboolahometest.ui.feed.FeedsItem;

import java.util.List;

interface MainViewState {
}

class Loading implements MainViewState {
}

class Error implements MainViewState {
    @NonNull
    private final Exception exception;

    Error(@NonNull final Exception exception) {
        this.exception = exception;
    }

    @NonNull
    public Exception getException() {
        return exception;
    }
}

class Success implements MainViewState {
    @NonNull
    private final List<FeedsItem> items;

    Success(@NonNull final List<FeedsItem> items) {
        this.items = items;
    }

    @NonNull
    public List<FeedsItem> getFeedItems() {
        return items;
    }
}