package com.example.taboolahometest.data.source;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.util.List;

public interface FeedDataSource {
    @NonNull
    List<Feed> getFeeds() throws IOException;
}
