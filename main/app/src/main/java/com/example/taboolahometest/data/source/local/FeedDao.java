package com.example.taboolahometest.data.source.local;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FeedDao {

    @NonNull
    @Query("SELECT * FROM feedlocal")
    List<FeedLocal> getAll();

    @Insert
    void insertAll(FeedLocal... feedLocals);
}
