package com.example.taboolahometest.ui.feed;

import androidx.annotation.NonNull;

import com.example.taboolahometest.data.source.Feed;

public class FeedItem implements FeedsItem {
    @NonNull
    private final Feed feed;

    public FeedItem(@NonNull final Feed feed) {
        this.feed = feed;
    }

    @NonNull
    public Feed getFeed() {
        return feed;
    }
}
