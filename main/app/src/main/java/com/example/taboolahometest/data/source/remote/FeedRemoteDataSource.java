package com.example.taboolahometest.data.source.remote;

import androidx.annotation.NonNull;

import com.example.taboolahometest.data.source.FeedDataSource;
import com.example.taboolahometest.data.source.Feed;

import java.io.IOException;
import java.util.List;

public class FeedRemoteDataSource implements FeedDataSource {
    @NonNull
    private final FeedApi feedApi;
    @NonNull
    private final FeedRemoteMapper feedRemoteMapper;

    public FeedRemoteDataSource(@NonNull final FeedApi feedApi, @NonNull final FeedRemoteMapper feedRemoteMapper) {
        this.feedApi = feedApi;
        this.feedRemoteMapper = feedRemoteMapper;
    }

    @NonNull
    @Override
    public List<Feed> getFeeds() throws IOException {
        final List<FeedRemote> feedRemotes = feedApi.loadFeeds().execute().body();
        if (feedRemotes == null) {
            throw new IOException("Error loading feeds from API.");
        }
        return feedRemoteMapper.fromRemotes(feedRemotes);
    }
}
