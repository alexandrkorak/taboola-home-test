package com.example.taboolahometest.ui.feed.decorator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

import com.example.taboolahometest.data.source.Feed;
import com.example.taboolahometest.ui.feed.FeedsItem;
import com.example.taboolahometest.ui.feed.TaboolaItem;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DefaultTaboolaFeedsDecoratorTest {
    private final String firstTaboolaItemId = "firstTaboolaItem";
    private final TaboolaItem firstTaboolaItem = new TaboolaItem(
            firstTaboolaItemId, firstTaboolaItemId, firstTaboolaItemId,
            firstTaboolaItemId, firstTaboolaItemId, firstTaboolaItemId
    );
    private final String secondTaboolaItemId = "secondTaboolaItem";
    private final TaboolaItem secondTaboolaItem = new TaboolaItem(
            secondTaboolaItemId, secondTaboolaItemId, secondTaboolaItemId,
            secondTaboolaItemId, secondTaboolaItemId, secondTaboolaItemId
    );
    @Mock
    TaboolaItemProvider taboolaItemProvider;
    private TaboolaFeedsDecorator taboolaFeedsDecorator;

    @Before
    public void setUp() {
        taboolaFeedsDecorator = new DefaultTaboolaFeedsDecorator(taboolaItemProvider);

        doReturn(3).when(taboolaItemProvider).getFirstItemPosition();
        doReturn(firstTaboolaItem).when(taboolaItemProvider).getFirstItem();
        doReturn(10).when(taboolaItemProvider).getLastItemPosition();
        doReturn(secondTaboolaItem).when(taboolaItemProvider).getLastItem();
    }

    @Test
    public void shouldAddTaboolaItemsToEmptyList() {
        final List<Feed> feeds = new ArrayList<>();

        final List<FeedsItem> feedsItems = taboolaFeedsDecorator.decorateWithTaboola(feeds);

        assertEquals(2, feedsItems.size());
        assertEquals(firstTaboolaItem, feedsItems.get(0));
        assertEquals(secondTaboolaItem, feedsItems.get(1));
    }

    @Test
    public void shouldAddTaboolaItemsToNotEmptyList() {
        final List<Feed> feeds = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            final String feedId = "feedId" + i;
            feeds.add(new Feed(feedId, feedId, feedId));
        }

        final List<FeedsItem> feedsItems = taboolaFeedsDecorator.decorateWithTaboola(feeds);

        assertEquals(2 + feeds.size(), feedsItems.size());
        assertEquals(firstTaboolaItem, feedsItems.get(2));
        assertEquals(secondTaboolaItem, feedsItems.get(9));
    }
}